package rockPaperScissors;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    int gameLoop = 1;
    String humanChoice, computerChoice;
    
    
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
    	
    	while (gameLoop == 1) {
			// TODO: Implement Rock Paper Scissors
	    	System.out.println("Let's play round "+roundCounter);
	    	humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
	    	
	    	if(validateInput(humanChoice)) {
	    		System.out.println("I do not understand cardboard. Could you try again?");
	    		continue;
	    	}
	    	computerChoice = randomChoice();
	    	
	    	String choice_string = "Human chose "+humanChoice+ ", computer chose " +computerChoice+"."; 
	    	
	    	if (isWinner(humanChoice, computerChoice)) {
	    		      System.out.print(choice_string + " Human wins.");
	    		      humanScore += 1;
	    	}
	                  
	    	else if (isWinner(computerChoice, humanChoice)) {
	    		System.out.print(choice_string + " Computer wins.");
	            computerScore += 1;
	    	}
	    		
	        else {
	        	System.out.print(choice_string + " It's a tie");
	        }
	            
	    	System.out.println("\nScore: human "+humanScore+", computer "+computerScore);
	    	if (continuePlaying(readInput("Do you wish to continue playing? (y/n)?"))) {
	    		System.out.println("Bye bye :)");
	    		gameLoop = 0;
	    	}
	    	else {
	    		roundCounter += 1;
	    		continue;
	    	}
	    	break;

		}
	        
    }

    public boolean continuePlaying(String input) {
		// TODO Auto-generated method stub
    	if (input.equalsIgnoreCase("n")) {
    		return true;
    	}
		return false;
	}

	public boolean validateInput(String humanChoice) {
		// TODO Auto-generated method stub
		if (humanChoice.equalsIgnoreCase("rock") || humanChoice.equalsIgnoreCase("paper") || humanChoice.equalsIgnoreCase("scissors")) {
			return false;
		}
		else {
			return true;
		}
	}

	public boolean isWinner(String choice1 ,String choice2) {
    	    if (choice1 == "paper") {
    	    	return choice2 == "rock";
    	    }
    	        
    	    else if (choice1 == "scissors") {
    	        return choice2 == "paper";
    	    }
    	    else {
    	    	 return choice2 == "scissors";
    	    }
    	       

	}

	public String randomChoice() {
		int r = (int) (Math.random()*3);
        String name = new String [] {"rock","paper","scissors"}[r];
        return name;
	}

	/**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    

}
